# frozen_string_literal: true

module Linter
  class GenderedGenerics
    def self.analyze(text)
      result = OpenStruct.new(
        gendered_generics: [],
        trend: ''
      )
      wordlists.dig('problematic').each do |problematic_word|
        word = problematic_word['word']
        regex = /\b#{word}\b/i
        result.gendered_generics << problematic_word if text.scan(regex).any?
      end
      result
    end

    private

    def self.wordlists
      file_path = File.join(__dir__,'../../data/gendered_generics_wordlist.yml')
      @wordlists ||= YAML.load_file(file_path)
    end
  end
end
