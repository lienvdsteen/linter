# frozen_string_literal: true

module Linter
  class CLI
    def self.analyze(file_name)
      # first read the file
      text = File.read(file_name)
      result = Linter::GenderAssociation.analyze(text)
      puts result.inspect.colorize(:red)
      result = Linter::PronounAssociation.analyze(text)
      puts result.inspect.colorize(:red)
    end
  end
end
