# frozen_string_literal: true

module Linter
  class MindsetAssociation < BaseAssociation
    USE_FOR_JOB_ADS = true
    FULL_WORD = false

    def self.wordlists
      file_path = File.join(__dir__, '../../data/mindset_wordlist.yml')
      @wordlists ||= YAML.load_file(file_path)
    end

    def self.calculate_trend(result)
      case result.growth_coded_word_counts.values.sum - result.fixed_coded_word_counts.values.sum
      when 0
        'neutral'
      when 1..3
        'growth-coded'
      when 3..Float::INFINITY
        'strongly growth-coded'
      when -Float::INFINITY..-3
        'strongly fixed-coded'
      else
        'fixed-coded'
      end
    end

    def self.add_recommendation(result)
      recommendation_file.find { |rec| rec['type'] == 'mindset' }&.dig('trends')&.find { |trend| trend['result'] == result.trend }&.dig('recommendation')
    end
  end
end
