# frozen_string_literal: true

require 'spec_helper'

describe Linter::GenderedGenerics do
  let(:text) { 'The businessman was asked to man the desk.' }

  describe '.analyze' do
    it 'returns the gendered generics' do
      result = described_class.analyze(text)
      expect(result.gendered_generics.size).to eq(2)
    end
  end
end
