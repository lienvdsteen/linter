# frozen_string_literal: true

require 'spec_helper'

describe Linter::MisusedWords do
  let(:text) { 'He is a spreadsheet guru, he is such a spirit animal' }

  describe '.analyze' do
    it 'returns the misused words' do
      result = described_class.analyze(text)
      expect(result.misused_words.size).to eq(2)
    end
  end
end
